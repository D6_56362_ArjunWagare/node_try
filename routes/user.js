const express = require('express')
const db = require('../db')
const utils = require('../utils')
const router = express.Router()

router.get('/', (request, response) => {
    const query = `
      SELECT Name, EmailId, EmailId 
      FROM User
    `
    db.execute(query, (error, result) => {
      response.send(utils.createResult(error, result))
    })
})

router.post('/', (request, response) => {
    const { name, emailId, password } = request.body 
    const query = `
        INSERT INTO User 
         (Name, EmailId, Password)
        VALUES
        ('${name}', '${emailId}','${password}')
    `
    db.execute(query, (error, result) => {
      response.send(utils.createResult(error, result))
    })
})

router.put('/:id', (request, response) => {
    const { id } = request.params
    const { name, emailId, password } = request.body 
    const query = `
        UPDATE User
        SET 
         Name = '${name}', EmailId = '${emailId}', Password = '${password}'
        WHERE
         Id = '${id}'
    `
    db.execute(query, (error, result) => {
      response.send(utils.createResult(error, result))
    })
})

router.delete('/:id', (request, response) => {
    const { id } = request.params
    const query = `
        DELETE FROM User
        WHERE
         Id = '${id}'
    `
    db.execute(query, (error, result) => {
      response.send(utils.createResult(error, result))
    })
})

  module.exports = router