const express = require('express')
const cors = require('cors')
const routerCategory = require('./routes/user')

const app = express()
app.use(cors('*'))
app.use(express.json())
app.use('/user', routerCategory)

app.listen(4000, '0.0.0.0', () => {
  console.log('category-server started on port 4000')
})


